﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Exception
{
    internal class UserException : System.Exception
    {
        public UserException()
        {

        }
        public UserException(string msg):base(msg)
        {

        }
    }
}
