﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal interface IFile
    {
        void WriteContentsToFile(User user,string fileName);
        List<string> ReadContentsFromFile(string fileName);
        bool IsUserNameExists(string userName,string filename);
    }
}
