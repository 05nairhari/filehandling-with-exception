﻿using FileHandling.Exception;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepo : IUserRepo,IFile
    {
        List<User> users;

        public UserRepo()
        {
            users = new List<User>();
        }

        public bool IsUserNameExists(string userName,string filename)
        {
            bool isUserAvailable = false;
            using (StreamReader sr = new StreamReader(filename))
            {
                string rowLine;
                while((rowLine=sr.ReadLine())!=null)
                {
                    string[] rowSplittedValues = rowLine.Split(',');
                    //foreach(string value in rowSplittedValues)
                    //{

                    if (rowSplittedValues[1] == userName)
                        {
                            isUserAvailable = true;
                        throw new UserException("ALREADY EXISTS");

                        break;
                    }
                }

            }
            return isUserAvailable;
        }
      
        public List<string> ReadContentsFromFile(string fileName)
        {
            //List<string> rowValues = new List<string>();
            //using (StreamReader sr = new StreamReader(fileName))
            //  string rowLine;
            //    while((rowLine=sr.ReadLine())!=null)
            //    {
            //    rowValues.Add(rowLine);

            //}
            //return rowValues;
            throw new NotImplementedException();    
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            bool isUserNameExists = IsUserNameExists(user.Name,fileName);
            if(isUserNameExists)
            {
                return false;
            }
            else
            {
                WriteContentsToFile(user,fileName);
                return true;

            }
            
        }

        public void WriteContentsToFile(User user,string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName,true))  ///true is used for appending bool
            {
                sw.WriteLine($"{user}");
            }
        }

        
    }
}
